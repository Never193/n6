function price() {
let s = document.getElementsByName("prodType");
let select = s[0];
let price = 0;
let prices = getPrices();
let priceIndex = parseInt(select.value) - 1;
if (priceIndex >= 0) {
price = prices.prodTypes[priceIndex];
}

let radioDiv = document.getElementById("radios");
/*radioDiv.style.display = (select.value == "2" ? "block" : "none");*/
let radios = document.getElementsByName("prodOptions");
radios.forEach(function(radio) {
if (radio.checked) {
let optionPrice = prices.prodOptions[radio.value];
if (optionPrice !== undefined) {
price += optionPrice;
}
}
});

let checkDiv = document.getElementById("checkboxes");
/*checkDiv.style.display = (select.value == "3" ? "block" : "none");*/
let checkboxes = document.querySelectorAll("#checkboxes input");
checkboxes.forEach(function(checkbox) {
if (checkbox.checked) {
let propPrice = prices.prodProperties[checkbox.name];
if (propPrice !== undefined) {
price += propPrice;
}
}
});

let prodPrice = document.getElementById("prodPrice");
let l = document.getElementById("kol").value;
if(l!=null && l>0){
prodPrice.innerHTML = price * l + " рублей";
}

function getPrices() {
return {
prodTypes: [0, 0, 0],
prodOptions: {
option1: 1,
option2: 5000,
option3: 700,
},
prodProperties: {
prop1: 50000,
prop2: 35000,
}
};
}

window.addEventListener('DOMContentLoaded', function (event) {
let radioDiv = document.getElementById("radios");
radioDiv.style.display = "none";
let s = document.getElementsByName("prodType");
let select = s[0];
select.addEventListener("change", function(event) {
let target = event.target;
console.log(target.value);
price();
});

let radios = document.getElementsByName("prodOptions");
radios.forEach(function(radio) {
radio.addEventListener("change", function(event) {
let v = event.target;
console.log(v.value);
price();
});
});

let checkboxes = document.querySelectorAll("#checkboxes input");
checkboxes.forEach(function(checkbox) {
checkbox.addEventListener("change", function(event) {
let c = event.target;
console.log(c.name);
console.log(c.value);
price();
});
});
price();
});
}
